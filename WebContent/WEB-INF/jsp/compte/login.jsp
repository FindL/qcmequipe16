<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Equipe 16 | Connexion</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">
  <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">
  <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link rel="stylesheet" href="<%= request.getContextPath() %>/assets/css/pixeladmin.min.css">
  <link rel="stylesheet" href="<%= request.getContextPath() %>/assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<%= request.getContextPath() %>/assets/css/themes/default.min.css">
  
  <script src="<%= request.getContextPath() %>/assets/pace/pace.min.js"></script>
  
  <style>
    .page-signin-modal {
      position: relative;
      top: auto;
      right: auto;
      bottom: auto;
      left: auto;
      z-index: 1;
      display: block;
    }

    .page-signin-form-group { position: relative; }

    .page-signin-icon {
      position: absolute;
      line-height: 21px;
      width: 36px;
      border-color: rgba(0, 0, 0, .14);
      border-right-width: 1px;
      border-right-style: solid;
      left: 1px;
      top: 9px;
      text-align: center;
      font-size: 15px;
    }

    html[dir="rtl"] .page-signin-icon {
      border-right: 0;
      border-left-width: 1px;
      border-left-style: solid;
      left: auto;
      right: 1px;
    }

    html:not([dir="rtl"]) .page-signin-icon + .page-signin-form-control { padding-left: 50px; }
    html[dir="rtl"] .page-signin-icon + .page-signin-form-control { padding-right: 50px; }

    #page-signin-forgot-form {
      display: none;
    }

    /* Margins */

    .page-signin-modal > .modal-dialog { margin: 30px 10px; }

    @media (min-width: 544px) {
      .page-signin-modal > .modal-dialog { margin: 60px auto; }
    }
  </style>
  
</head>
<body>

  <div class="page-signin-modal modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="box m-a-0">
          <div class="box-row">

            <div class="box-cell col-md-5 bg-primary p-a-4">
              <div class="text-xs-center text-md-left">
                <a class="px-demo-brand px-demo-brand-lg" href="index.html"><span class="px-demo-logo bg-primary m-t-0"><span class="px-demo-logo-1"></span><span class="px-demo-logo-2"></span><span class="px-demo-logo-3"></span><span class="px-demo-logo-4"></span><span class="px-demo-logo-5"></span><span class="px-demo-logo-6"></span><span class="px-demo-logo-7"></span><span class="px-demo-logo-8"></span><span class="px-demo-logo-9"></span></span><span class="font-size-20 line-height-1">Equipe 16</span></a>
                <div class="font-size-15 m-t-1 line-height-1">Projet QCM</div>
              </div>
            </div>

            <div class="box-cell col-md-7">

              <!-- Sign In form -->
			<form action="<%= request.getContextPath() %>/login" method="post" class="p-a-4" id="page-signin-form">
                <h4 class="m-t-0 m-b-4 text-xs-center font-weight-semibold">Se connecter</h4>

			    <c:if test="${not empty erreur}">
			        <label class="control-label" for="inputError">
			        	<i class="fa fa-times-circle-o"></i> 
			        	<c:out value="${erreur}"/></label>
			      </c:if>
			      <div class="form-group has-feedback <c:if test="${not empty erreurEmail}">has-error</c:if>">
			      <c:if test="${not empty erreurEmail}">
			        <label class="control-label" for="inputError">
			        	<i class="fa fa-times-circle-o"></i> 
			        	<c:out value="${erreurEmail}"/></label>
			      </c:if>
			        <input type="email" class="form-control" name="email" required placeholder="Email">
			        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
			      </div>
			      <div class="form-group has-feedback">
			      <c:if test="${not empty erreurPassword}">
			        <label class="control-label" for="inputError">
			        	<i class="fa fa-times-circle-o"></i> 
			        	<c:out value="${erreurPassword}"/></label>
			      </c:if>
			        <input type="password" class="form-control" name="password" required placeholder="Password">
			        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
			      </div>

                <button type="submit" name="submit" value="submit" class="btn btn-block btn-lg btn-primary m-t-3">Connexion</button>
              </form>              
            </div>
          </div>
        </div>
      </div>

      <div class="text-xs-center m-t-2 font-weight-bold font-size-14 text-white" id="px-demo-signup-link">
        Pas encore membre ? <a href="<%= request.getContextPath() %>/inscription" class="text-center">S'enregistrer</a>
      </div>
    </div>
  </div>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<%= request.getContextPath() %>/assets/js/bootstrap.min.js"></script>
<script src="<%= request.getContextPath() %>/assets/js/pixeladmin.min.js"></script>

<script>

</script>
</body>
</html>
